#ifndef LQMEASSTUDIO_PROCESSPLUGIN
#define LQMEASSTUDIO_PROCESSPLUGIN

#include <QObject>
#include <QSettings>
#include <QStringList>

namespace LQMeasStudio {
    /* Базовый класс для плагинов обработки данных.
       Используется для общего вызова запуска и останова обработки, загрузки
       и сохранения настроек плагина и т.п. */
    class ProcessPlugin : public QObject {
        Q_OBJECT
    public:
        ProcessPlugin(QString name, bool optional = false, QObject *parent = 0);


        virtual void procStartPrepare() {}
        virtual void procStart() {}
        virtual void procStopRequest() {}
        virtual void procStop() {}
        virtual void procClear() {}
        virtual void retranslate() {}


        virtual QStringList procPluginDependencyNames() const {return QStringList();}

        void procSetEnabled(bool enabled);
        bool procEnabled() const {return m_enabled && m_aviable;}


        void procLoadSettings(QSettings &set);

        void procSaveSettings(QSettings &set);

        void procCheckAviable();

    protected:
        virtual void procProtLoadSettings(QSettings &set) {}
        virtual void procProtSaveSettings(QSettings &set) {}
        virtual void procProtSetEnabled(bool enabled) {}
    private:

        void enableStateChanged(bool enabled);

        bool m_optional;
        bool m_enabled;
        bool m_aviable;
    };
}



#endif // LQMEASSTUDIO_PROCESSPLUGIN

