#include "BlockTimeGraphPlugin.h"
#include "LQMeasStudio.h"
#include "LQMeasStudioPlot.h"


namespace LQMeasStudio {
    BlockTimeGraphPlugin::BlockTimeGraphPlugin(QObject *parent) :
        DataBlockProcessPlugin(typePluginName(), true, parent) {

        m_plot = new Plot();

        LQMeasStudioEnv->mainWidgetLout()->addWidget(m_plot);
    }

    void BlockTimeGraphPlugin::procStartPrepare() {
        foreach (QSharedPointer<ChannelInfo> ch, LQMeasStudioEnv->channelsConfig()->channels()) {
            QCPGraph *timeGraph = m_plot->addGraph();
            timeGraph->setName(ch->uniqueName());
            timeGraph->setPen(ch->color());

            connect(ch.data(), SIGNAL(visibilityChanged(bool)),
                    SLOT(onChannelVisabilityChanged(bool)));
        }
    }

    void BlockTimeGraphPlugin::procStop() {

    }

    void BlockTimeGraphPlugin::procClear() {
        m_plot->clearGraphs();
        m_plot->requestRelpot();
    }


    void BlockTimeGraphPlugin::retranslate() {
        m_plot->xAxis->setLabel(tr("Time, ms"));
        m_plot->yAxis->setLabel(tr("Amplitude, V"));
        m_plot->requestRelpot();
    }

    void BlockTimeGraphPlugin::blockProcess(QSharedPointer<DataBlock> block) {
        QCPDataMap *data = new QCPDataMap();
        for (int i=0; i < block->values().size(); i++) {
            double y = block->values().at(i);
            double x = 1000.*(block->dt()*i + block->channel()->timeShift());
            data->insert(x, QCPData(x,y));
        }
        block->setParameter(param_timeGraphData(), QVariant::fromValue((void*)data));
    }

    void BlockTimeGraphPlugin::blockShowResult(QSharedPointer<DataBlock> block) {
        if (block->hasParameter(param_timeGraphData())) {
            QCPDataMap *data = static_cast<QCPDataMap *>(block->parameter(param_timeGraphData()).value<void*>());
            int idx = block->channel()->num();
            if (idx < m_plot->graphCount()) {
                m_plot->graph(idx)->setData(data);
                m_plot->updateAxisAutoScale();
                m_plot->requestRelpot();
            } else {
                delete data;
            }
            block->removeParameter(param_timeGraphData());
        }
    }

    void BlockTimeGraphPlugin::blockDropResult(QSharedPointer<DataBlock> block) {
        if (block->hasParameter(param_timeGraphData())) {
            QCPDataMap *data = static_cast<QCPDataMap *>(block->parameter(param_timeGraphData()).value<void*>());
            delete data;
            block->removeParameter(param_timeGraphData());
        }
    }

    void BlockTimeGraphPlugin::procProtLoadSettings(QSettings &set) {
        PlotConfigDialog::Config defcfg;
        defcfg.x.autoScale = true;
        defcfg.x.min = 0;
        defcfg.x.max = 100;
        defcfg.y.autoScale = true;
        defcfg.y.min = -10;
        defcfg.y.max = 10;
        m_plot->loadConfig(set, "timePlot", &defcfg);
        m_plot->setCfgDecimals(3,5);

        m_plot->xAxis->grid()->setSubGridVisible(true);
    }

    void BlockTimeGraphPlugin::procProtSaveSettings(QSettings &set) {
        m_plot->saveConfig(set);
    }

    void BlockTimeGraphPlugin::onChannelVisabilityChanged(bool visible) {
        ChannelInfo *info = qobject_cast<ChannelInfo*>(sender());
        if (info) {
            int idx = info->num();
            if (idx < m_plot->graphCount()) {
                m_plot->graph(idx)->setVisible(visible);
                m_plot->requestRelpot();
            }
        }
    }

}

