#ifndef X502DOUTPINSTATESDIALOG_H
#define X502DOUTPINSTATESDIALOG_H

#include <QDialog>

namespace Ui {
class X502DoutPinStatesDialog;
}
class QLedIndicator;

class X502DoutPinStatesDialog : public QDialog {
    Q_OBJECT

public:
    explicit X502DoutPinStatesDialog(QWidget *parent, unsigned value, int ch_cnt);
    ~X502DoutPinStatesDialog();

    unsigned currentValue() const;
private Q_SLOTS:
    void updateOutIndicators(unsigned value);
    void onAsyncDoutPinChanged();
    void onAsyncDoutHexChanged();
private:
    struct AsyncPinState {
        int pin_num;
        QLedIndicator *led;

        AsyncPinState(int init_pin, QLedIndicator *init_led) :
            pin_num(init_pin), led(init_led) {}
    } ;

    void addDoutPin(int pin_num, QString name);

    Ui::X502DoutPinStatesDialog *ui;
    QList<AsyncPinState> m_asyncDoutStates;
    bool m_async_dout_update;
};

#endif // X502DOUTPINSTATESDIALOG_H
