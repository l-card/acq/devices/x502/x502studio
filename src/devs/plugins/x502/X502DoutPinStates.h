#ifndef X502DOUTPINSTATES_H
#define X502DOUTPINSTATES_H

#include <QDialog>

namespace Ui {
class X502DoutPinStates;
}

class X502DoutPinStates : public QDialog
{
    Q_OBJECT

public:
    explicit X502DoutPinStates(QWidget *parent = nullptr);
    ~X502DoutPinStates();

private:
    Ui::X502DoutPinStates *ui;
};

#endif // X502DOUTPINSTATES_H
