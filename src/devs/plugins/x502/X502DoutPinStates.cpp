#include "X502DoutPinStates.h"
#include "ui_X502DoutPinStates.h"

X502DoutPinStates::X502DoutPinStates(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::X502DoutPinStates)
{
    ui->setupUi(this);
}

X502DoutPinStates::~X502DoutPinStates()
{
    delete ui;
}
