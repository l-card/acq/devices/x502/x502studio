#ifndef LQMEASSTUDIO_ENVIRONMENT_H
#define LQMEASSTUDIO_ENVIRONMENT_H

#include "ProcessPlugin.h"
#include "in/InDataChannelsConfig.h"
#include "devs/DeviceTree.h"
#include "in/InReceiveLauncher.h"
#include "out/OutSigGenaration.h"

#include <QVBoxLayout>

#define LQMeasStudioEnv  LQMeasStudio::Environment::instance()
#define LQMeasStudioPlugin(typeName) dynamic_cast<typeName*>(LQMeasStudio::Environment::instance()->plugin(#typeName))

namespace LQMeasStudio {
    class ChannelSignalParamPanel;
    class NetworkBrowserModel;
    class IpAddrConfig;

    class Environment {
    public:

        QVBoxLayout *mainWidgetLout() const {return m_mainWgtLout;}
        ChannelSignalParamPanel *chSignalParamsPanel() const {return m_chSigPanel;}
        InDataChannelsConfig *channelsConfig() {return &m_inChCfg;}
        DeviceTree *deviceTree() const {return m_devTree;}
        InReceiveLauncher *inLauncher() {return &m_inLauncher;}
        OutSigGenaration  *outGenerator() {return &m_outGen;}
        IpAddrConfig *ipConfig() const {return m_ipConfig;}
        NetworkBrowserModel *netBrowser() const {return m_netBrowser;}

        QList<ProcessPlugin *> processPlugings() const {return m_procPlugings;}

        ProcessPlugin *plugin(QString typeName) const;


        static Environment *instance();

        void loadSettings(QSettings &set);
        void saveSettings(QSettings &set) const;

    public slots:
        void addPlugin(ProcessPlugin *plugin) {m_procPlugings.append(plugin);}
    private:
        Environment() : m_chSigPanel(0), m_netBrowser(0) {}


        void init(DeviceTree *devtree, QVBoxLayout *mainWgtLout);

        ~Environment();



        ChannelSignalParamPanel *m_chSigPanel;
        DeviceTree *m_devTree;
        InReceiveLauncher m_inLauncher;
        OutSigGenaration  m_outGen;
        InDataChannelsConfig m_inChCfg;
        QVBoxLayout *m_mainWgtLout;

        QList<ProcessPlugin *> m_procPlugings;
        NetworkBrowserModel *m_netBrowser;
        IpAddrConfig *m_ipConfig;



        friend class MainWindow;
    };
}

#endif // LQMEASSTUDIO_ENVIRONMENT_H
