<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>BlackfinFirmwareSelection</name>
    <message>
        <location filename="../src/devs/plugins/x502/BlackfinFirmwareSelection.h" line="11"/>
        <source>Blackfin firmware file selection</source>
        <translation>Выбор файла прошивки Blackfin</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/BlackfinFirmwareSelection.h" line="12"/>
        <source>Firmware files</source>
        <translation>Файлы прошивок</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/BlackfinFirmwareSelection.h" line="12"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
</context>
<context>
    <name>ChannelSignalParamPanel</name>
    <message>
        <source>Signal parameters</source>
        <translation type="obsolete">Параметры сигналов</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="obsolete">Устройство</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation type="obsolete">Канал</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Цвет</translation>
    </message>
</context>
<context>
    <name>DacSignalDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_DacSignalDialog.h" line="114"/>
        <source>Signal Parameters</source>
        <translation>Параметры сигнала</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_DacSignalDialog.h" line="115"/>
        <source>Signal Type</source>
        <translation>Тип сигнала</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_DacSignalDialog.h" line="116"/>
        <source>Preset</source>
        <translation>Предустановка</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalDialog.cpp" line="43"/>
        <source>First signal point</source>
        <translation>Первая точка сигнала</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalDialog.cpp" line="44"/>
        <source>Specified value</source>
        <translation>Указанное значение</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalDialog.cpp" line="45"/>
        <source>Don&apos;t change</source>
        <translation>Не изменять</translation>
    </message>
</context>
<context>
    <name>DacSignalParamUiConst</name>
    <message>
        <source> V</source>
        <translation type="obsolete"> В</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiConst.cpp" line="29"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>DacSignalParamUiPulse</name>
    <message>
        <source>Hz</source>
        <translation type="obsolete">Гц</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiPulse.cpp" line="27"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiPulse.cpp" line="35"/>
        <source>Duty Cycle</source>
        <translation>Коэффициент заполнения</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiPulse.cpp" line="43"/>
        <source>Pulse amplitude</source>
        <translation>Амплитуда импульса</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiPulse.cpp" line="51"/>
        <source>Pause amplitude</source>
        <translation>Амплитуда паузы</translation>
    </message>
</context>
<context>
    <name>DacSignalParamUiRamp</name>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiRamp.cpp" line="27"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiRamp.cpp" line="35"/>
        <source>Initial Amplitude</source>
        <translation>Начальная амплитуда</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiRamp.cpp" line="43"/>
        <source>End Amplitude</source>
        <translation>Конечная амплитуда</translation>
    </message>
</context>
<context>
    <name>DacSignalParamUiSin</name>
    <message>
        <source> Hz</source>
        <translation type="obsolete"> Гц</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiSin.cpp" line="29"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <source> V</source>
        <translation type="obsolete"> B</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiSin.cpp" line="37"/>
        <source>Amplitude</source>
        <translation>Амплитуда</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiSin.cpp" line="45"/>
        <source>Offset</source>
        <translation>Смещение</translation>
    </message>
    <message>
        <location filename="../src/out/DacSignalParams/DacSignalParamUiSin.cpp" line="51"/>
        <source>Phase</source>
        <translation>Фаза</translation>
    </message>
</context>
<context>
    <name>FreqConfigWidget</name>
    <message>
        <source>Hz</source>
        <translation type="vanished">Гц</translation>
    </message>
    <message>
        <source>KHz</source>
        <translation type="vanished">КГц</translation>
    </message>
    <message>
        <source>MHz</source>
        <translation type="vanished">МГц</translation>
    </message>
</context>
<context>
    <name>GraphConfigDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="163"/>
        <source>Graphic settings</source>
        <translation>Настройки графика</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="164"/>
        <source>X axe</source>
        <translation>Ось X</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="165"/>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="169"/>
        <source>Autoscale</source>
        <translation>Автомасштаб</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="166"/>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="170"/>
        <source>Minimum value</source>
        <translation>Минимальное значение</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="167"/>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="171"/>
        <source>Maximum value</source>
        <translation>Максимальное значение</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_PlotConfigDialog.h" line="168"/>
        <source>Y axe</source>
        <translation>Ось Y</translation>
    </message>
</context>
<context>
    <name>InReceiveLauncher</name>
    <message>
        <source>Channel </source>
        <translation type="obsolete">Канал</translation>
    </message>
    <message>
        <source>Data acquisition error</source>
        <translation type="obsolete">Ошибка сбора данных</translation>
    </message>
    <message>
        <source>Error occured during data acquisition</source>
        <translation type="obsolete">Произошла ошибка во время сбора данных</translation>
    </message>
</context>
<context>
    <name>IpAddrAddDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_IpAddrAddDialog.h" line="87"/>
        <source>IP address addition</source>
        <translation>Добавление IP-адреса</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_IpAddrAddDialog.h" line="88"/>
        <source>IP address</source>
        <translation>IP-адрес</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_IpAddrAddDialog.h" line="91"/>
        <source>Device type</source>
        <translation>Тип устройства</translation>
    </message>
</context>
<context>
    <name>IpAddrConfig</name>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP-адрес</translation>
    </message>
    <message>
        <source>Device type</source>
        <translation type="obsolete">Тип устройства</translation>
    </message>
</context>
<context>
    <name>IpAddrConfigDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_IpAddrConfigDialog.h" line="102"/>
        <source>IP address configuration</source>
        <translation>Настройка IP-адресов</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_IpAddrConfigDialog.h" line="103"/>
        <source>Add IP address</source>
        <translation>Добавить IP-адрес</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_IpAddrConfigDialog.h" line="104"/>
        <source>Remove IP address</source>
        <translation>Удалить IP-адрес</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::BlockTimeGraphPlugin</name>
    <message>
        <location filename="../src/plot/BlockTimeGraphPlugin.cpp" line="37"/>
        <source>Time, ms</source>
        <translation>Время, мс</translation>
    </message>
    <message>
        <location filename="../src/plot/BlockTimeGraphPlugin.cpp" line="38"/>
        <source>Amplitude, V</source>
        <translation>Амплитуда, В</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::ChannelSignalParamPanel</name>
    <message>
        <location filename="../src/ChannelSignalParamPanel.cpp" line="68"/>
        <source>Signal parameters</source>
        <translation>Параметры сигналов</translation>
    </message>
    <message>
        <location filename="../src/ChannelSignalParamPanel.cpp" line="70"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../src/ChannelSignalParamPanel.cpp" line="70"/>
        <source>Channel</source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="../src/ChannelSignalParamPanel.cpp" line="70"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::DeviceTree</name>
    <message>
        <location filename="../src/devs/DeviceTree.cpp" line="115"/>
        <source>Refresh list of devices...</source>
        <translation>Обновление списка устройств...</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::InReceiveLauncher</name>
    <message>
        <location filename="../src/in/InReceiveLauncher.cpp" line="23"/>
        <source>Channel </source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="../src/in/InReceiveLauncher.cpp" line="90"/>
        <source>Data acquisition error</source>
        <translation>Ошибка сбора данных</translation>
    </message>
    <message>
        <location filename="../src/in/InReceiveLauncher.cpp" line="91"/>
        <source>Error occured during data acquisition</source>
        <translation>Произошла ошибка во время сбора данных</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::IpAddrConfig</name>
    <message>
        <location filename="../src/devs/IpAddrConfig/IpAddrConfig.cpp" line="76"/>
        <source>IP address</source>
        <translation>IP-адрес</translation>
    </message>
    <message>
        <location filename="../src/devs/IpAddrConfig/IpAddrConfig.cpp" line="79"/>
        <source>Device type</source>
        <translation>Тип устройства</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::MainWindow</name>
    <message>
        <location filename="../src/MainWindow.cpp" line="141"/>
        <source>Application started</source>
        <translation>Приложение запущено</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="200"/>
        <source>Device discovery error</source>
        <translation>Ошибка поиска устройств</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="200"/>
        <source>Error occured during device discovery</source>
        <translation>Во время поиска устройств в локальной сети возникла ошибка</translation>
    </message>
    <message>
        <source>Output stream buffer underflow occured</source>
        <translation type="vanished">Произошло опустошение буфера потокового вывода </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="206"/>
        <source>Output generation buffer underflow occured</source>
        <translation>Произошло опустошение буфера синхронного вывода</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="258"/>
        <source>Start error</source>
        <translation>Ошибка запуска</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="258"/>
        <source>No device has been started</source>
        <translation>Ни одного устройства не было запущено</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="314"/>
        <location filename="../src/MainWindow.cpp" line="323"/>
        <source>Saving screen image</source>
        <translation>Сохранение изображения экрана</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="321"/>
        <location filename="../src/MainWindow.cpp" line="426"/>
        <location filename="../src/MainWindow.cpp" line="430"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="321"/>
        <source>Cannot save screen image to file!</source>
        <translation>Не удалось сохранить изображение экрана в файл!</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="323"/>
        <source>Screen image saved successfully</source>
        <translation>Изображение экрана сохранено успешно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="353"/>
        <source>Save application configuration</source>
        <translation>Сохранение конфигурации приложения</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="362"/>
        <source>Configuration was saved succefully</source>
        <translation>Конфигурация была сохранена успешно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="364"/>
        <source>Save configuration error</source>
        <translation>Ошибка сохранения конфигурации</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="370"/>
        <source>Load application configuration</source>
        <translation>Загрузка конфигурации приложения</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="376"/>
        <source>Open configuration file error</source>
        <translation>Ошибка открытия файла конфигурации</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="379"/>
        <source>Configuration was loaded successfully</source>
        <translation>Конфигурация загружена успешно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="405"/>
        <source>Application configuration</source>
        <translation>Конфигурация приложения</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="411"/>
        <location filename="../src/MainWindow.cpp" line="424"/>
        <source>Saving data block</source>
        <translation>Сохранение блока данных</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="424"/>
        <source>Data block was saved successfully</source>
        <translation>Блок данных был сохранен успешно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="426"/>
        <source>Cannot open file for write!</source>
        <translation>Не удалось открыть файл для записи!</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="430"/>
        <source>No data to save!</source>
        <translation>Нет данных для сохранения!</translation>
    </message>
    <message>
        <source>Frequency, Hz</source>
        <translation type="obsolete">Частота, Гц</translation>
    </message>
    <message>
        <source>Spectrum, dB</source>
        <translation type="obsolete">Спектр, dB</translation>
    </message>
    <message>
        <source>Time, ms</source>
        <translation type="obsolete">Время, мс</translation>
    </message>
    <message>
        <source>Amplitude, V</source>
        <translation type="obsolete">Амплитуда, В</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::NetworkBrowserModel</name>
    <message>
        <location filename="../src/devs/NetworkDevicesBrowser/NetworkBrowserModel.cpp" line="196"/>
        <source>Instance name</source>
        <translation>Имя экземпляра</translation>
    </message>
    <message>
        <location filename="../src/devs/NetworkDevicesBrowser/NetworkBrowserModel.cpp" line="199"/>
        <source>Device name</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../src/devs/NetworkDevicesBrowser/NetworkBrowserModel.cpp" line="202"/>
        <source>Device serial</source>
        <translation>Серийный номер</translation>
    </message>
    <message>
        <location filename="../src/devs/NetworkDevicesBrowser/NetworkBrowserModel.cpp" line="205"/>
        <source>IP address</source>
        <translation>IP-адрес</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::OutSigGenaration</name>
    <message>
        <location filename="../src/out/OutSigGenaration.cpp" line="15"/>
        <source>Data setup error</source>
        <translation>Ошибка установки данных</translation>
    </message>
    <message>
        <location filename="../src/out/OutSigGenaration.cpp" line="15"/>
        <source>Error occured during output generation initialization</source>
        <translation>Произошла ошибка во время инициализации генерации вывода данных</translation>
    </message>
    <message>
        <location filename="../src/out/OutSigGenaration.cpp" line="36"/>
        <source>Error occured during output generation start</source>
        <translation>Произошла ошибка во время запуска генерации вывода данных</translation>
    </message>
    <message>
        <source>Error occured during out stream initialization</source>
        <translation type="vanished">Во время инициализации потока вывода данных произошла ошибка</translation>
    </message>
    <message>
        <source>Error occured during setup cycle signal data</source>
        <translation type="vanished">Во время установки циклического сигнала на вывод произошла ошибка</translation>
    </message>
    <message>
        <location filename="../src/out/OutSigGenaration.cpp" line="36"/>
        <location filename="../src/out/OutSigGenaration.cpp" line="76"/>
        <source>Data generation error</source>
        <translation>Ошибка генерации данных</translation>
    </message>
    <message>
        <location filename="../src/out/OutSigGenaration.cpp" line="76"/>
        <source>Error occured during data generation</source>
        <translation>Во время генерации данных произошла ошибка</translation>
    </message>
</context>
<context>
    <name>LQMeasStudio::SpectrumGraphPlugin</name>
    <message>
        <location filename="../src/plot/SpectrumGraphPlugin.cpp" line="43"/>
        <source>Frequency, Hz</source>
        <translation>Частота, Гц</translation>
    </message>
    <message>
        <location filename="../src/plot/SpectrumGraphPlugin.cpp" line="44"/>
        <source>Spectrum, dB</source>
        <translation>Спектр, dB</translation>
    </message>
</context>
<context>
    <name>LQTableCopyAction</name>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Копировать</translation>
    </message>
    <message>
        <source>Copy selected text</source>
        <translation type="vanished">Копировать выделенный текст</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="270"/>
        <source>&amp;Device Configuration...</source>
        <translation>&amp;Конфигурация устройства...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="271"/>
        <source>&amp;Start</source>
        <translation>&amp;Пуск</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="276"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="278"/>
        <source>St&amp;op</source>
        <translation>Ос&amp;танов</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="283"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="285"/>
        <source>&amp;Refresh device list</source>
        <translation>&amp;Обновить список устройств</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="287"/>
        <source>Ctrl+Shift+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="289"/>
        <source>&amp;Application settings...</source>
        <translation>&amp;Настройки приложения...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="291"/>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="343"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="293"/>
        <source>&amp;Close application</source>
        <translation>&amp;Закрыть приложение</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="295"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="297"/>
        <source>&amp;Log</source>
        <translation>&amp;Журнал</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="301"/>
        <source>&amp;Signal parameters</source>
        <translation>Па&amp;раметры сигналов</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="309"/>
        <source>&amp;Configure connection IP addresses...</source>
        <translation>Настройка &amp;IP-адресов для подключения...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="313"/>
        <source>Sa&amp;ve screenshot</source>
        <translation>Сохра&amp;нить изображение экрана</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="314"/>
        <source>&amp;Save data block</source>
        <translation>Сохранить блок &amp;данных</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="315"/>
        <source>&amp;Modify Interface Configuration...</source>
        <translation>&amp;Изменить настройки интерфейса...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="316"/>
        <source>&amp;Update Firmware...</source>
        <translation>Обновить про&amp;шивку...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="317"/>
        <source>&amp;System</source>
        <translation>&amp;Системный</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="321"/>
        <source>&amp;Russian</source>
        <translation>&amp;Русский</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="325"/>
        <source>&amp;English</source>
        <translation>&amp;Английский</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="329"/>
        <source>&amp;Browse Local Network Devices...</source>
        <translation>&amp;Просмотр устройств в локальной сети...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="333"/>
        <source>Asynchronous Input/Output...</source>
        <translation>Асинхронный ввод/вывод...</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="334"/>
        <source>Sa&amp;ve configuration</source>
        <translation>&amp;Сохранение конфигурации</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="336"/>
        <source>Save configuration</source>
        <translation>Сохранение конфигурации</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="341"/>
        <source>&amp;Load configuration</source>
        <translation>&amp;Загрузка конфигурации</translation>
    </message>
    <message>
        <source>New configuration</source>
        <translation type="vanished">Новая конфигурация</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="351"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Конфигурация</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="339"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="345"/>
        <source>&amp;New configuration</source>
        <translation>&amp;Новая конфигурация</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="352"/>
        <source>&amp;Devices panel</source>
        <translation>&amp;Панель устройств</translation>
    </message>
    <message>
        <source>Asynchronous Input/Output</source>
        <translation type="vanished">Асинхронный ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="346"/>
        <source>Data ac&amp;quisition</source>
        <translation>Сбор &amp;данных</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="347"/>
        <source>De&amp;vices</source>
        <translation>&amp;Устройства</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="348"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="349"/>
        <source>Wi&amp;ndows</source>
        <translation>&amp;Окна</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="350"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <source>De&amp;vices panel</source>
        <translation type="vanished">Панель &amp;устройств</translation>
    </message>
    <message>
        <source>Devices Panel</source>
        <translation type="vanished">Панель устройств</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Файл</translation>
    </message>
    <message>
        <source>Update Firmware</source>
        <translation type="obsolete">Обновление прошивки модуля...</translation>
    </message>
    <message>
        <source>Device Configuration...</source>
        <translation type="vanished">Конфигурация устройства...</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Пуск</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="273"/>
        <source>Start data acquisition/generation</source>
        <translation>Запуск сбора/генерации данных</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Останов</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="280"/>
        <source>Stop data acquisition/generation</source>
        <translation>Останов сбора/генерации данных</translation>
    </message>
    <message>
        <source>Refresh device list</source>
        <translation type="vanished">Обновить список устройств</translation>
    </message>
    <message>
        <source>Application settings...</source>
        <translation type="vanished">Настройки приложения...</translation>
    </message>
    <message>
        <source>Log</source>
        <translation type="vanished">Журнал</translation>
    </message>
    <message>
        <source>Signal parameters</source>
        <translation type="vanished">Параметры сигналов</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="299"/>
        <source>Show log panel</source>
        <translation>Показать панель журнала</translation>
    </message>
    <message>
        <source>Close application</source>
        <translation type="vanished">Закрыть приложение</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="303"/>
        <source>Show panel with signal parameters</source>
        <translation>Показать панель с параметрами сигнала</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="305"/>
        <source>&amp;Devices Panel</source>
        <translation>Панель &amp;устройств</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="307"/>
        <source>Show panel with devices tree</source>
        <translation>Показать панель с деревом подключенных устройств</translation>
    </message>
    <message>
        <source>Configure connection IP addresses</source>
        <translation type="obsolete">Настройка IP-адресов для подключения</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="311"/>
        <source>Configure connection IP addresses...</source>
        <translation>Настройка IP-адресов для подключения...</translation>
    </message>
    <message>
        <source>Save screenshot</source>
        <translation type="vanished">Сохранить изображение экрана</translation>
    </message>
    <message>
        <source>Save data block</source>
        <translation type="vanished">Сохранить блок данных</translation>
    </message>
    <message>
        <source>Modify Interface Configuration...</source>
        <translation type="vanished">Изменить настройки интерфейса...</translation>
    </message>
    <message>
        <source>Update Firmware...</source>
        <translation type="vanished">Обновить прошивку...</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">Системный</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="319"/>
        <source>Set system language</source>
        <translation>Установить язык интерфейса, соответствующий языку системы </translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="323"/>
        <source>Set Russian language</source>
        <translation>Установить русский язык интерфейса</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Английский</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="327"/>
        <source>Set English language</source>
        <translation>Установить английский язык интерфейса</translation>
    </message>
    <message>
        <source>Browse Local Network Devices</source>
        <translation type="obsolete">Просмотр устройств в локальной сети</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_MainWindow.h" line="331"/>
        <source>Browse Local Network Devices...</source>
        <translation>Просмотр устройств в локальной сети...</translation>
    </message>
    <message>
        <source>Data acquisition</source>
        <translation type="vanished">Сбор данных</translation>
    </message>
    <message>
        <source>Devices</source>
        <translation type="vanished">Устройства</translation>
    </message>
    <message>
        <source>Windows</source>
        <translation type="vanished">Окна</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Язык</translation>
    </message>
    <message>
        <source>Devices panel</source>
        <translation type="vanished">Панель устройств</translation>
    </message>
    <message>
        <source>Time, ms</source>
        <translation type="obsolete">Время, мс</translation>
    </message>
    <message>
        <source>Frequency, Hz</source>
        <translation type="obsolete">Частота, Гц</translation>
    </message>
    <message>
        <source>Application started</source>
        <translation type="obsolete">Приложение запущено</translation>
    </message>
    <message>
        <source>Data acquisition error</source>
        <translation type="obsolete">Ошибка сбора данных</translation>
    </message>
    <message>
        <source>Error occured during data acquisition</source>
        <translation type="obsolete">Произошла ошибка во время сбора данных</translation>
    </message>
    <message>
        <source>%1: error occured during data acquisition: %2</source>
        <translation type="obsolete">%1: Во время сбора данных  возникла ошибка возникала: %2</translation>
    </message>
    <message>
        <source>Channel </source>
        <translation type="obsolete">Канал</translation>
    </message>
    <message>
        <source>Saving screen image</source>
        <translation type="obsolete">Сохранение изображения экрана</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>Cannot save screen image to file!</source>
        <translation type="obsolete">Не удалось сохранить изображение экрана в файл!</translation>
    </message>
    <message>
        <source>Screen image saved successfully</source>
        <translation type="obsolete">Изображение экрана сохранено успешно</translation>
    </message>
    <message>
        <source>Saving data block</source>
        <translation type="obsolete">Сохранение блока данных</translation>
    </message>
    <message>
        <source>Data block was saved successfully</source>
        <translation type="obsolete">Блок данных был сохранен успешно</translation>
    </message>
    <message>
        <source>Cannot open file for write!</source>
        <translation type="obsolete">Не удалось открыть файл для записи!</translation>
    </message>
    <message>
        <source>No data to save!</source>
        <translation type="obsolete">Нет данных для сохранения!</translation>
    </message>
    <message>
        <source>Spectrum, dB</source>
        <translation type="obsolete">Спектр, dB</translation>
    </message>
    <message>
        <source>Amplitude, V</source>
        <translation type="obsolete">Амплитуда, В</translation>
    </message>
</context>
<context>
    <name>NetworkBrowserDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_NetworkBrowserDialog.h" line="69"/>
        <source>Local Network Devices</source>
        <translation>Устройства в локальной сети</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_NetworkBrowserDialog.h" line="70"/>
        <source>Enable network devices discovery</source>
        <translation>Разрешить поиск устройств в сети</translation>
    </message>
</context>
<context>
    <name>OptionalFileSelection</name>
    <message>
        <source>Choice of firmware file</source>
        <translation type="obsolete">Выбор файла с прошивкой</translation>
    </message>
</context>
<context>
    <name>OutSigGenaration</name>
    <message>
        <source>Data generation error</source>
        <translation type="obsolete">Ошибка генерации данных</translation>
    </message>
    <message>
        <source>Error occured during data generation</source>
        <translation type="obsolete">Во время генерации данных произошла ошибка</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_SettingsDialog.h" line="100"/>
        <source>Application Settings</source>
        <translation>Настройки приложения</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_SettingsDialog.h" line="101"/>
        <source>ADC data processing</source>
        <translation>Обработка данных АЦП</translation>
    </message>
    <message>
        <source> ms</source>
        <translation type="obsolete"> мс</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_SettingsDialog.h" line="103"/>
        <source>Processing block time</source>
        <translation>Время блока для обработки</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_SettingsDialog.h" line="104"/>
        <source>Processing block interval</source>
        <translation>Интервал между блоками для обработки</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_SettingsDialog.h" line="106"/>
        <source>Spectral Processing</source>
        <translation>Спектральная обработка</translation>
    </message>
</context>
<context>
    <name>StdUnits</name>
    <message>
        <source>Hz</source>
        <translation type="obsolete">Гц</translation>
    </message>
    <message>
        <source>KHz</source>
        <translation type="obsolete">КГц</translation>
    </message>
    <message>
        <source>MHz</source>
        <translation type="obsolete">МГц</translation>
    </message>
</context>
<context>
    <name>TableCopyAction</name>
    <message>
        <location filename="../lib/QTableViewHelpers/actions/TableCopyAction.cpp" line="65"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../lib/QTableViewHelpers/actions/TableCopyAction.cpp" line="66"/>
        <source>Copy selected text</source>
        <translation>Копировать выделенный текст</translation>
    </message>
</context>
<context>
    <name>WaitDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_WaitDialog.h" line="54"/>
        <source>Waiting...</source>
        <translation>Ожидание...</translation>
    </message>
</context>
<context>
    <name>X502AsyncIoDialog</name>
    <message>
        <location filename="../src/devs/plugins/x502/X502AsyncIoDialog.cpp" line="78"/>
        <source>Write digital outputs</source>
        <translation>Запись значения цифровых выходов</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502AsyncIoDialog.cpp" line="93"/>
        <source>Read digital inputs</source>
        <translation>Чтение значения цифровых входов</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502AsyncIoDialog.cpp" line="125"/>
        <source>Write DAC value</source>
        <translation>Запись значения ЦАП</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502AsyncIoDialog.cpp" line="170"/>
        <source>Output to DAC%1</source>
        <translation>Вывод на ЦАП%1</translation>
    </message>
    <message>
        <source>Digital lines control</source>
        <translation type="vanished">Управление цифровыми линиями</translation>
    </message>
    <message>
        <source>Asynchronous inputs and outputs</source>
        <translation type="vanished">Асинхронный ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="152"/>
        <source>Asynchronous Input/Output</source>
        <translation>Асинхронный ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="153"/>
        <source>Digital lines</source>
        <translation>Цифровые линии</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="154"/>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="155"/>
        <source>HEX Value</source>
        <translation>HEX код</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="156"/>
        <source>Output</source>
        <translation>Вывод</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="157"/>
        <source>Input</source>
        <translation>Ввод</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502AsyncIoDialog.h" line="158"/>
        <source>DAC</source>
        <translation>ЦАП</translation>
    </message>
</context>
<context>
    <name>X502ConfigDialog</name>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="52"/>
        <source>DAC</source>
        <translation>ЦАП</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="53"/>
        <source>Galvanic decoupling</source>
        <translation>Гальваническая развязка</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="54"/>
        <source>BlackFin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="56"/>
        <source>Ethernet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="57"/>
        <source>Industrial</source>
        <translation>Индустриальное исп</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="68"/>
        <source>Channel %1</source>
        <translation>Канал %1</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="81"/>
        <source>All Channels</source>
        <translation>Все каналы</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="306"/>
        <source>Comm. mode</source>
        <translation>С общ. землей</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="308"/>
        <source>Diff.</source>
        <translation>Дифф.</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="310"/>
        <source>Zero</source>
        <translation>Изм. нуля</translation>
    </message>
    <message>
        <source>Â± 10 V</source>
        <translation type="obsolete">± 10 В</translation>
    </message>
    <message>
        <source>Â± 5 V</source>
        <translation type="obsolete">± 5 В</translation>
    </message>
    <message>
        <source>Â± 2 V</source>
        <translation type="obsolete">± 2 В</translation>
    </message>
    <message>
        <source>Â± 1 V</source>
        <translation type="obsolete">± 1 В</translation>
    </message>
    <message>
        <source>Â± 0.5 V</source>
        <translation type="obsolete">± 0.5 В</translation>
    </message>
    <message>
        <source>Â± 0.2 V</source>
        <translation type="obsolete">± 0.2 В</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="331"/>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="549"/>
        <source>Off</source>
        <translation>Откл</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="181"/>
        <source>Cycle</source>
        <translation>Циклический</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="182"/>
        <source>Stream</source>
        <translation>Потоковый</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="285"/>
        <source>Internal</source>
        <translation>Внутренний</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="286"/>
        <source>From external master</source>
        <translation>От внешнего мастера</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="287"/>
        <source>SYN1 Rise</source>
        <translation>По фронту SYN1</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="288"/>
        <source>SYN2 Rise</source>
        <translation>По фронту SYN2</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="289"/>
        <source>SYN1 Fall</source>
        <translation>По спаду SYN1</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="290"/>
        <source>SYN2 Fall</source>
        <translation>По спаду SYN2</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502ConfigDialog.cpp" line="407"/>
        <source>ADC configuration error</source>
        <translation>Ошибка настройки АЦП</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="534"/>
        <source>Device configuration</source>
        <translation>Конфигурация устройства</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="535"/>
        <source>Device Information</source>
        <translation>Информация о устройстве</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="538"/>
        <source>FPGA firmware version</source>
        <translation>Версия прошивки ПЛИС</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="536"/>
        <source>PLDA firmware version</source>
        <translation>Версия прошивки PLDA</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="537"/>
        <source>ARM firmware version</source>
        <translation>Версия прошивки ARM</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="539"/>
        <source>Serial Number</source>
        <translation>Серийный номер</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="540"/>
        <source>Input frequency</source>
        <translation>Частота ввода</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="541"/>
        <source>Adc frequency</source>
        <translation>Частота АЦП</translation>
    </message>
    <message>
        <source> Hz</source>
        <translation type="obsolete"> Гц</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="542"/>
        <source>Channel frequency</source>
        <translation>Частота на канал</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="543"/>
        <source>Use max channel frequency</source>
        <translation>Использовать максимальную частоту на канал</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="545"/>
        <source>Sync Mode</source>
        <translation>Режим синхронизации</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="546"/>
        <source>Sync frequency mode</source>
        <translation>Режим генерации частоты синхронизации</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="547"/>
        <source>Sync start mode</source>
        <translation>Режим запуска частоты синхронизации</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="548"/>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="549"/>
        <source>Reference frequency</source>
        <translation>Опорная частота</translation>
    </message>
    <message>
        <source>Digital Lines</source>
        <translation type="vanished">Цифровые линии</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="550"/>
        <source>Pull resistors</source>
        <translation>Подтягивающие резисторы</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="551"/>
        <source>DI1-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="552"/>
        <source>DI9-16</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="553"/>
        <source>DI_SYN1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="554"/>
        <source>DI_SYN2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="555"/>
        <source>CONV_IN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="556"/>
        <source>START_IN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="557"/>
        <source>DAC settings</source>
        <translation>Настройки ЦАП</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="558"/>
        <source>Out Frequency</source>
        <translation>Частота вывода</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="559"/>
        <source>Out mode</source>
        <translation>Режим вывода</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="560"/>
        <source>Channel 1</source>
        <translation>Канал 1</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="561"/>
        <source>Channel 2</source>
        <translation>Канал 2</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="562"/>
        <source>Digital outputs</source>
        <translation>Цыфровые выходы</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="563"/>
        <source>Enable</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="564"/>
        <source>Value (HEX)</source>
        <translation>Значение (HEX)</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="565"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <source>Enable Async Output</source>
        <translation type="vanished">Разрешение асинхр. вывода</translation>
    </message>
    <message>
        <source>Output value (HEX)</source>
        <translation type="vanished">Выводимое значение (HEX)</translation>
    </message>
    <message>
        <source>Output value</source>
        <translation type="vanished">Выводимое значение</translation>
    </message>
    <message>
        <source>Pins config...</source>
        <translation type="vanished">Настройки выходов...</translation>
    </message>
    <message>
        <source>HEX Value</source>
        <translation type="obsolete">HEX код</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="566"/>
        <source>Blackfin firmware</source>
        <translation>Прошивка Blackfin</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="567"/>
        <source>ADC Channels</source>
        <translation>Каналы АЦП</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="569"/>
        <source>Channel</source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="571"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="573"/>
        <source>Range</source>
        <translation>Диапазон</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502ConfigDialog.h" line="575"/>
        <source>Average</source>
        <translation>Усреднение</translation>
    </message>
</context>
<context>
    <name>X502DevPlugin</name>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="75"/>
        <source>Read interface configuration</source>
        <translation>Чтение конфигурации интерфейса</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="76"/>
        <source>Cannot read interface configuration: %1</source>
        <translation>Не удалось прочитать конфигурацию интерфейса: %1</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="116"/>
        <source>Get interface parameters</source>
        <translation>Получение параметров интерфейса</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="117"/>
        <source>Cannot get interface parameters: %1</source>
        <translation>Не удалось получить параметры интерфейса: %1</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="156"/>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="160"/>
        <source>Write interface configuration</source>
        <translation>Запись конфигурации интерфейса</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="157"/>
        <source>Cannot write interface configuration: Error %1: %2</source>
        <translation>Не удалось записать конфигурацию интерфейса. Ошибка %1: %2</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="161"/>
        <source>Interface configuration was written successfully</source>
        <translation>Настройки интерфейса были записаны успешно</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="190"/>
        <source>Choice of firmware file</source>
        <translation>Выбор файла с прошивкой</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="191"/>
        <source>Firmware files</source>
        <translation>Файлы прошивок</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="191"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="220"/>
        <source>Switch to bootloader</source>
        <translation>Переход в режим загрузчика</translation>
    </message>
    <message>
        <location filename="../src/devs/plugins/x502/X502DevPlugin.cpp" line="221"/>
        <source>Cannot switch device to bootloader: %1</source>
        <translation>Не удалось перевести устройство в режим загрузчика: %1</translation>
    </message>
</context>
<context>
    <name>X502DigConfigDialog</name>
    <message>
        <source>Asynchronous inputs and outputs</source>
        <translation type="vanished">Асинхронный ввод/вывод</translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="vanished">Вывод</translation>
    </message>
    <message>
        <source>HEX Value</source>
        <translation type="vanished">HEX код</translation>
    </message>
    <message>
        <source>Digital lines control</source>
        <translation type="vanished">Управление цифровыми линиями</translation>
    </message>
    <message>
        <source>Input</source>
        <translation type="vanished">Ввод</translation>
    </message>
    <message>
        <source>Write digital outputs</source>
        <translation type="vanished">Запись значения цифровых выходов</translation>
    </message>
    <message>
        <source>Read digital inputs</source>
        <translation type="vanished">Чтение значения цифровых входов</translation>
    </message>
</context>
<context>
    <name>X502DoutPinStatesDialog</name>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502DoutPinStatesDialog.h" line="78"/>
        <source>Digital Output Pin States</source>
        <translation>Состояния цифровых выходов</translation>
    </message>
    <message>
        <location filename="../../../_builds/X502Studio/debug/ui_X502DoutPinStatesDialog.h" line="79"/>
        <source>HEX Value</source>
        <translation>HEX код</translation>
    </message>
</context>
</TS>
